<p align="center">
    <a href="https://sylius.com" target="_blank">
        <img src="https://demo.sylius.com/assets/shop/img/logo.png" />
    </a>
</p>

<h1 align="center">Sylius Template</h1>

<p align="center">This is Sylius Standard Edition repository for starting new projects.</p>

Documentation
-------------

- [Symfony](https://symfony.com/doc/current/index.html)
- [Sylius](http://docs.sylius.com)
- [Doctrine](https://www.doctrine-project.org/projects/doctrine-orm/en/current/tutorials/getting-started.html)
- [Sylius Shop API](https://app.swaggerhub.com/apis/Sylius/sylius-shop-api/1.0.0) (Sylius plugin for headless shop)

Environment
----------

```bash
$ cp .env .env.local
```
For SQL connection: 
`` DATABASE_URL=mysql://user:password@hostname/db_name ``

Installation
------------

```bash
$ composer install
$ yarn install
$ yarn build
$ php bin/console sylius:install
$ php bin/console sylius:fixture:load template_ru
$ php bin/console server:start -d
$ open http://localhost:8000/
```

Troubleshooting
---------------

If something goes wrong, errors & exceptions are logged at the application level:

```bash
$ tail -f var/log/prod.log
$ tail -f var/log/dev.log
or 
$ php bin/console server:log
```
